#!/usr/local/bin/zsh

#-
# Copyright (c) 2024 Shawn Webb <shawn.webb@hardenedbsd.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

portsdir="/usr/ports"
searchopt=""

function usage() {
	echo "USAGE: ${1} [-p portsdir] -o OPTION" >&2
	exit 1
}

while getopts "o:p:" o; do
	case "${o}" in
		o)
			searchopt="${OPTARG}"
			;;
		p)
			portsdir="${OPTARG}"
			;;
	esac
done

if [ -z "${searchopt}" ]; then
	usage ${0}
fi

categories=$(cd ${portsdir}; make -V SUBDIR)
for category in $(echo ${categories}); do
	cd ${portsdir}/${category}
	for port in $(make -V SUBDIR); do
		(
			cd ${portsdir}/${category}/${port}
			val=$(make showconfig BATCH=1 | grep -F "${searchopt}=on:")
			if [ ! -z "${val}" ]; then
				echo ${category}/${port}
			fi
		) &
	done
done
