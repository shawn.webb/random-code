#!/usr/bin/env zsh

DRILL="/usr/bin/drill"
NC="/usr/bin/nc"

family=""
host=""
nameserver=""
port="22"

function resolve_host() {
	local preargs=""

	preargs="${family}"

	${DRILL} -Q \
		${=preargs} \
		${host} \
		${nameserver}
	return ${?}
}

while getopts '46d:h:' o; do
	case "${o}" in
		4)
			family="-t a"
			;;
		6)
			family="-t aaaa"
			;;
		d)
			nameserver="${OPTARG}"
			;;
		h)
			host="${OPTARG}"
			;;
		p)
			port="${OPTARG}"
			;;
	esac
done

if [ -z "${host}" ]; then
	echo "[-] Specify host with -h" >&2
	exit 1
fi

ip=$(resolve_host)
res=${?}
if [ ${res} -gt 0 ]; then
	exit ${res}
fi

if [ -z "${ip}" ]; then
	echo "[-] Host ${host} not found" >&2
	exit 1
fi

${NC} ${ip} ${port}
