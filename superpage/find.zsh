#!/usr/bin/env zsh

print_error() {
	echo ${1} >& 2
}

if [ $(id -u) -gt 0 ]; then
	print_error "[-] Please run as UID 0"
	exit 1
fi

for p in $(ps -axo pid | sed 1d); do
	mappings=$(procstat -v ${p} 2>/dev/null | sed 1d)
	found=0
	echo ${mappings} | while read -n line; do
		if awk '{print $9;}' | grep -qF S; then
			found=1
			break
		fi
	done
	if [ ${found} -gt 0 ]; then
		ps wwxu -p ${p} | sed 1d
	fi
done
