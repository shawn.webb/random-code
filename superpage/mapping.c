#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/mman.h>

#define DEFAULTSZ	2097152

int
main(int argc, char *argv[])
{
	void *mapping;

	mapping = mmap(NULL, DEFAULTSZ, PROT_READ, MAP_ALIGNED_SUPER | MAP_ANON | MAP_SHARED, -1, 0);
	if (mapping == MAP_FAILED) {
		perror("mmap");
		exit(1);
	}

	printf("%d %p\n", getpid(), mapping);
	while (1) {
		sleep(60);
	}

	return (0);
}
