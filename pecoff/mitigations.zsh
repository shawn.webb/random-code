#!/usr/bin/env zsh
#-
# Copyright (c) 2023 Shawn Webb <shawn.webb@hardenedbsd.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Dependencies:
# 1. winchecksec
# 2. jq
# 3. zsh

self=$(realpath $(dirname ${0}))

dir=""
ofile=""

function usage() {
	echo "USAGE: ${self} -d /path/to/directory -o output_file.json" >&2
	exit 1
}

while getopts 'd:o:' o; do
	case "${o}" in
		d)
			dir="${OPTARG}"
			;;
		o)
			ofile="${OPTARG}"
			;;
	esac
done

if [ -z "${dir}" ]; then
	usage
fi

if [ ! -d ${dir} ]; then
	echo "[-] Directory ${dir} does not exist" >&2
	exit 1
fi

if [ -z "${ofile}" ]; then
	usage
fi

truncate -s 0 ${ofile}
res=${?}
if [ ${res} -gt 0 ]; then
	exit ${res}
fi

echo "[" > ${ofile}

find ${dir} -iname '*.dll' -o -iname '*.exe' | while read f; do
	winchecksec --json ${f} > ${ofile}.tmp
	res=${?}
	if [ ${res} -gt 0 ]; then
		rm -f ${ofile}.tmp
		continue
	fi

	fsize=$(wc -c ${ofile}.tmp | awk '{print $1;}')
	if [ ${fsize} -lt 3 ]; then
		rm -f ${ofile}.tmp
		continue
	fi

	# Skip first and last bytes
	dd "if=${ofile}.tmp" "of=${ofile}.tmp2" \
		bs=1 \
		count=$((${fsize} - 3)) \
		skip=1
	cat ${ofile}.tmp2 >> ${ofile}

	echo -e "\n," >> ${ofile}
	rm -f ${ofile}.tmp
	rm -f ${ofile}.tmp2
done
sed '$d' ${ofile} > ${ofile}.tmp
mv ${ofile}.tmp ${ofile}
echo "]" >> ${ofile}
