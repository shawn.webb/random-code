/*-
 * Copyright (c) 2023, by Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

int
main(int argc, char *argv[], char **envp)
{
	int err, fd, mfd;
	struct stat sb;
	void *buf;

	if (argc < 2) {
		fprintf(stderr, "USAGE: %s /path/to/program [args]\n",
		    argv[0]);
		return (1);
	}

	fd = open(argv[1], O_RDONLY | O_CLOEXEC);
	if (fd == -1) {
		perror("open");
		exit(1);
	}

	memset(&sb, 0, sizeof(sb));
	if (fstat(fd, &sb)) {
		perror("fstat");
		exit(1);
	}

	mfd = memfd_create("memfdexec", 0);
	if (mfd == -1) {
		perror("memfd_create");
		exit(1);
	}

	if (ftruncate(mfd, sb.st_size)) {
		perror("ftruncate");
		exit(1);
	}

	buf = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if (buf == MAP_FAILED) {
		perror("mmap");
		exit(1);
	}

	if (write(mfd, buf, sb.st_size) != sb.st_size) {
		perror("write");
		exit(1);
	}

	if (lseek(mfd, 0, SEEK_SET)) {
		perror("lseek");
		exit(1);
	}
	err = fexecve(mfd, &argv[1], envp);
	perror("fexecve");
	return (err);
}
