#!/usr/bin/env zsh
#
# Copyright (c) 2024 Shawn Webb <shawn.webb@hardenedbsd.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

GITLEAKS="${HOME}/go/bin/gitleaks"

redirpath="/dev/null"
rootpath=""
resultspath=""
verbose=0

while getopts "d:g:r:v" o; do
	case "${o}" in
		g)
			GITLEAKS="${OPTARG}"
			;;
		d)
			rootpath=$(realpath ${OPTARG})
			;;
		r)
			resultspath=$(realpath ${OPTARG})
			;;
		v)
			verbose=$((${verbose} + 1))
			;;
	esac
done

if [ -z "${rootpath}" ] || [ ! -d ${rootpath} ]; then
	echo "[-] Specify rootpath with -d" >&2
	exit 1
fi

if [ -z "${resultspath}" ] || [ ! -d ${resultspath} ]; then
	echo "[-] Specify resultspath with -r" >&2
	exit 1
fi

if [ ${verbose} -gt 1 ]; then
	redirpath="/dev/stdout"
fi

find ${rootpath} -maxdepth 1 -type d | while read repo; do
	if [ ! -d ${repo}/.git ]; then
		continue
	fi

	result_file=$(basename ${repo})
	result_path="${resultspath}/${result_file}.json"

	if [ ${verbose} -gt 0 ]; then
		echo "[*] Checking ${repo}"
	fi

	${GITLEAKS} detect \
		-s ${repo} \
		-r ${result_path} \
		-f json > ${redirpath} 2>&1
	res=${?}
	if [ ${res} -ne 0 ]; then
		echo "[-] ${repo} failed. Exit code: ${res}" >&2
		echo "    => Log: ${result_path}" >&2
	fi
done
