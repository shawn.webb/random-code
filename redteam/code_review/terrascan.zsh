#!/usr/bin/env zsh

TERRASCAN="/home/shawn/installs/bin/terrascan"

rootpath=""
resultspath=""

while getopts "d:r:t:" o; do
	case "${o}" in
		d)
			rootpath=$(realpath ${OPTARG})
			;;
		r)
			resultspath=$(realpath ${OPTARG})
			;;
		t)
			TERRASCAN="${OPTARG}"
			;;
	esac
done

if [ -z "${rootpath}" ] || [ ! -d ${rootpath} ]; then
	echo "[-] Specify rootpath with -d" >&2
	exit 1
fi

if [ -z "${resultspath}" ] || [ ! -d ${resultspath} ]; then
	echo "[-] Specify resultspath with -d" >&2
	exit 1
fi

find ${rootpath} -maxdepth 1 -type d | while read repo; do
	if [ ! -d ${repo}/.git ]; then
		continue
	fi

	result_file=$(basename ${repo})

	pushd ${repo}
	${TERRASCAN} scan \
		-o json > ${resultspath}/terrascan-${result_file}.json
	popd
done
