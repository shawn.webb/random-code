#!/usr/bin/env zsh

repodir=""

while getopts 'd:' o; do
	case "${o}" in
		d)
			repodir="${OPTARG}"
			;;
	esac
done

if [ -z "${repodir}" ]; then
	echo "[-] Specify the path to the directory with -d" >&2
	exit 1
fi

if [ ! -d "${repodir}" ]; then
	echo "[-] ${repodir} must be a directory" >&2
	exit 1
fi

cd ${repodir}

find . -maxdepth 1 -type d | while read d; do
	if [ ! -d "${d}/.git" ]; then
		continue
	fi

	echo "===> ${d}"
	(
		cd ${d}
		git pull
	)
done
