#!/usr/bin/env zsh

function is_deletable() {
	local beactive

	beactive="${1}"

	echo ${beactive} | grep -q -- "R"
	res=${?}
	if [ ${res} -eq 0 ]; then
		return 1
	fi

	echo ${beactive} | grep -q -- "N"
	res=${?}
	if [ ${res} -eq 0 ]; then
		return 1
	fi

	return 0
}

if [ $(id -u) -gt 0 ]; then
	echo "[-] Please run this as root" >&2
	exit 1
fi

nop=0
verbose=0

while getopts 'nv' o; do
	case "${o}" in
		n)
			nop=1
			;;
		v)
			verbose=$((${verbose} + 1))
			;;
	esac
done

bectl list -H | while read -n line; do
	bename=$(echo ${line} | awk '{print $1;}')
	beactive=$(echo ${line} | awk '{print $2;}')

	if ! is_deletable ${beactive}; then
		continue
	fi

	if [ ${verbose} -gt 0 ]; then
		echo "Deleting ${bename}"
	fi

	if [ ${nop} -eq 1 ]; then
		continue
	fi

	bectl destroy -Fo ${bename}
	res=${?}
	if [ ${res} -gt 0 ]; then
		exit 1
	fi
done
